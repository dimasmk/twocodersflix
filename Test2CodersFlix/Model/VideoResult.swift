//
//  VideoResult.swift
//  Test2C
//
//  Created by Dimitar Shopovski on 14.12.21.
//

import Foundation

struct VideoResult: Decodable {

    let name: String
    let key: String
    let site: String
    let size: UInt
    let type: String
    let official: Bool
    let publishedAt: String
    let id: String

    enum CodingKeys: String, CodingKey {
        case name, key, site
        case size, type, official, id
        case publishedAt = "published_at"
    }
}

struct VideoResponse: Decodable {
    let id: UInt
    let results: [VideoResult]
}
