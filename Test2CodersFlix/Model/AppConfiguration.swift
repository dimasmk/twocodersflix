//
//  AppConfiguration.swift
//  Test2C
//
//  Created by Dimitar Shopovski on 13.12.21.
//

import Foundation

struct ImageConfiguration: Decodable {
    let baseUrl: String
    let secureBaseUrl: String
    let backdropSizes: [String]
    let logoSizes: [String]
    let posterSizes: [String]
    let stillSizes, profileSizes: [String]

    enum CodingKeys: String, CodingKey {
        case baseUrl = "base_url"
        case secureBaseUrl = "secure_base_url"
        case backdropSizes = "backdrop_sizes"
        case logoSizes = "logo_sizes"
        case posterSizes = "poster_sizes"
        case stillSizes = "still_sizes"
        case profileSizes = "profile_sizes"
    }
}

struct AppConfiguration: Decodable {
    let images: ImageConfiguration
    let changeKeys: [String]

    enum CodingKeys: String, CodingKey {
        case images
        case changeKeys = "change_keys"
    }
}
