//
//  Movie.swift
//  Test2C
//
//  Created by Dimitar Shopovski on 13.12.21.
//

import Foundation

struct Movie: Decodable {
    let adult: Bool?
    let backdropPath: String?
    let genreIds: [Int]
    let originalLanguage: String
    let originalTitle: String
    let posterPath: String
    let title: String
    let video: Bool?
    let voteAverage: Double
    let id: UInt
    let overview: String
    let releaseDate: String
    let voteCount: UInt
    let popularity: Double
    let mediaType: MediaType

    enum CodingKeys: String, CodingKey {
        case adult
        case backdropPath = "backdrop_path"
        case genreIds = "genre_ids"
        case originalLanguage = "original_language"
        case originalTitle = "original_title"
        case posterPath = "poster_path"
        case video, title, id, overview, popularity
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
        case mediaType = "media_type"
        case releaseDate = "release_date"
    }

}

extension Movie {
    var votesLabelText: String {
        return "Rating: \(voteAverage) from \(voteCount) votings"
    }    
}

enum MediaType: String, Decodable {
    case movie
    case all
}

struct MovieResponse: Decodable {
    let page: Int
    let results: [Movie]
    let totalPages: Int
    let totalResults: Int

    enum CodingKeys: String, CodingKey {
        case page, results
        case totalPages = "total_pages"
        case totalResults = "total_results"
    }
}
