//
//  2CodersAPIManager.swift
//  Test2C
//
//  Created by Dimitar Shopovski on 13.12.21.
//

import Foundation
import UIKit

class TwoCodersAPIManager {
    static let shared = TwoCodersAPIManager()
    var imageConfig: ImageConfiguration?
    let apiKey: String = "82972e305ad65d3c6205b584bc6820c6"
    let baseUrl: String

    private init() {
        #if Production
        baseUrl = "http://api.themoviedb.org/3/"
        #elseif Development
        baseUrl = "http://api.themoviedb.org/3/"
        #else
        baseUrl = "http://api.themoviedb.org/3/"
        #endif
    }

    func fetchDecodable<T: Decodable>(urlString: String, completion: @escaping (Result<T, Error>) -> Void) {
        guard let url = URL(string: urlString) else { return } // or throw an error
        URLSession.shared.dataTask(with: url) { (data, _, error) in
            if let error = error { completion(.failure(error)); return }
            completion( Result { try JSONDecoder().decode(T.self, from: data!) })
        }.resume()
    }

    func fetchData(urlString: String, completion: @escaping (Result<UIImage?, Error>) -> Void) {
        guard let url = URL(string: urlString) else { return } // or throw an error
        URLSession.shared.dataTask(with: url) { (data, _, error) in
            if let error = error { completion(.failure(error)); return }
            completion( Result { UIImage(data: data!) })
        }.resume()
    }

    func getMovies(page: Int, completion: @escaping (MovieResponse?, Error?) -> Void) {

        let urlString = baseUrl + "trending/movie/day?api_key=" + apiKey + "&page=\(page)"
        fetchDecodable(urlString: urlString) { (result: Result<MovieResponse, Error>) in
            switch result {
            case .failure(let error):
                completion(nil, error)
            case .success(let movieResponse):
                completion(movieResponse, nil)
            }
        }
    }

    func getConfigurationData(completion: @escaping (AppConfiguration?, Error?) -> Void) {

        let urlString = baseUrl + "configuration?api_key=" + apiKey

        fetchDecodable(urlString: urlString) { (result: Result<AppConfiguration, Error>) in
            switch result {
            case .success(let config):
                self.imageConfig = config.images
                completion(config, nil)
            case .failure(let error):
                completion(nil, error)
            }
        }
    }

    func getVideoData(movieId: UInt, completion: @escaping ([VideoResult], Error?) -> Void) {
        let urlString = baseUrl + "movie/\(movieId)/videos?api_key=" + apiKey

        fetchDecodable(urlString: urlString) { (result: Result<VideoResponse, Error>) in
            switch result {
            case .success(let videoResponse):
                completion(videoResponse.results, nil)
            case .failure(let error):
                completion([], error)
            }
        }

    }
}
