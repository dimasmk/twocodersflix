//
//  ViewController.swift
//  Test2C
//
//  Created by Dimitar Shopovski on 12.12.21.
//

import UIKit

class TrendingMoviesViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var movies: [Movie] = []
    var searchMode = false
    var searchMovies: [Movie] = []
    
    var currentPage: Int = 1
    var totalPages: Int = 0
    var totalResults: UInt = 0
    
    var loadedPages: [Int] = []
    var loadingPage = false
    
    let flowLayout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 5
        layout.minimumLineSpacing = 5
        layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        return layout
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        baseAppearanceSettings()
    }

    class func instance() -> TrendingMoviesViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let controller = storyboard.instantiateViewController(withIdentifier: "trendingControllerId") as? TrendingMoviesViewController else {
            fatalError("Cannot isntantiate")
        }
        return controller
    }

    fileprivate func baseAppearanceSettings() {
        title = "Trending movies"
        activityIndicator.startAnimating()
        // swiftlint:disable all line_length
        collectionView.register(UINib(nibName: "MovieCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: MovieCollectionViewCell.cellIdentifier)
        self.load(page: currentPage)
    }

    fileprivate func load(page: Int) {
        
        if loadedPages.contains(page) || loadingPage {
            return
        }
        loadingPage = true
        TwoCodersAPIManager.shared.getMovies(page: page) { [weak self] list, error in
            guard error == nil, let this = self else {
                DispatchQueue.main.async {
                    self?.activityIndicator.stopAnimating()
                }
                self?.loadingPage = false
                return
            }
            guard let movieSection = list else {
                self?.loadingPage = false
                return
            }
            
            if page == 1 {
                this.initProperties(movieResponse: movieSection)
            }
            
            if !this.loadedPages.contains(page) {
                this.loadedPages.append(page)
            }
            this.loadingPage = false
            this.movies.append(contentsOf: movieSection.results)
            DispatchQueue.main.async {
                this.collectionView.reloadData()
                this.activityIndicator.stopAnimating()
            }
        }
    }
    
    // MARK: - Helper
    fileprivate func initProperties(movieResponse: MovieResponse) {
        totalPages = movieResponse.totalPages
        totalResults = UInt(movieResponse.totalResults)
    }
}

extension TrendingMoviesViewController: UICollectionViewDataSource {
    // MARK: - UICollectionViewDataSource

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return searchMode ? searchMovies.count : movies.count
    }
    // swiftlint:disable line_length
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == movies.count - 1 && currentPage < totalPages {
            currentPage += 1
            load(page: currentPage)
        }
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MovieCollectionViewCell.cellIdentifier, for: indexPath) as? MovieCollectionViewCell else {
             return UICollectionViewCell()
        }
        if searchMode {
            let movie = searchMovies[indexPath.row]
            cell.configure(movie: movie)
        } else {
            let movie = movies[indexPath.row]
            cell.configure(movie: movie)
        }
        cell.layoutIfNeeded()
        return cell
    }

    // swiftlint:disable line_length
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            // swiftlint:disable line_length
            guard let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "searchHeaderId", for: indexPath) as? CollectionSearchReusableView
            else {
                return UICollectionReusableView()
            }

             return headerView
         }

         return UICollectionReusableView()
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let movie = getMovie(at: indexPath)
        let detailsController = MovieDetailsViewController.instance(with: movie)
        self.navigationController?.pushViewController(detailsController, animated: true)
    }
    
    fileprivate func getMovie(at indexPath: IndexPath) -> Movie {
        if searchMode {
            return searchMovies[indexPath.row]
        }
        return movies[indexPath.row]
    }
}

extension TrendingMoviesViewController: UICollectionViewDelegateFlowLayout {

    // MARK: - UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        let numberOfItemsPerRow: CGFloat = 3
        let spacing: CGFloat = flowLayout.minimumInteritemSpacing
        let availableWidth = width - spacing * (numberOfItemsPerRow + 1)
        let itemDimension = floor(availableWidth / numberOfItemsPerRow)
        return CGSize(width: itemDimension, height: itemDimension + 50.0)
    }
}

extension TrendingMoviesViewController: UISearchBarDelegate {

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text, !text.isEmpty, text != "" else {
            return
        }
        self.searchMode = true
        self.searchMovies.removeAll()
        self.searchMovies = []
        self.searchMovies = searchMovies(text: text) ?? []
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            self.searchMode = false
            DispatchQueue.main.async {
                searchBar.searchTextField.becomeFirstResponder()
                self.collectionView.reloadData()
            }
        } else {
            self.searchMode = true
            self.searchMovies.removeAll()
            self.searchMovies = []
            self.searchMovies = searchMovies(text: searchText) ?? []
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("cancel")
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print("didend")
    }
    
    // MARK: - Helpers
    
    private func searchMovies(text: String) -> [Movie]? {
        let predicate = NSPredicate(format: "SELF CONTAINS %@", text)
        return self.movies.filter { movie in
            return predicate.evaluate(with: movie.title)
        }
    }
}
