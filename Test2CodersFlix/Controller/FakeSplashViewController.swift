//
//  FakeSplashViewController.swift
//  Test2C
//
//  Created by Dimitar Shopovski on 14.12.21.
//

import UIKit

class FakeSplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        loadConfigData()
    }

    fileprivate func loadConfigData() {
        TwoCodersAPIManager.shared.getConfigurationData { config, error in
            guard let _ = config?.images, error == nil else {
                self.showMainScreen()
                return
            }
            self.showMainScreen()
        }
    }

    fileprivate func showMainScreen() {
        DispatchQueue.main.async {
            guard let navigationController = self.navigationController else {
                return
            }
            let controller = TrendingMoviesViewController.instance()
            navigationController.viewControllers = [controller]
        }
    }
}
