//
//  MovieDetailsViewController.swift
//  Test2C
//
//  Created by Dimitar Shopovski on 13.12.21.
//

import UIKit
import youtube_ios_player_helper

class MovieDetailsViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var votingLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var trailerContainerView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var playerView: YTPlayerView!
    var movie: Movie?
    var videoIsPlaying: Bool = false

    class func instance(with movie: Movie) -> MovieDetailsViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        // swiftlint:disable line_length
        guard let controller = storyboard.instantiateViewController(identifier: "movieDetailsIdentifier") as? MovieDetailsViewController else {
            fatalError("Failed to initialize")
        }
        controller.movie = movie
        return controller
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        baseAppearanceSettings()
        setupMovie()
        prepareVideoTrailer()
    }

    fileprivate func baseAppearanceSettings() {
        view.backgroundColor = .black
        playerView.delegate = self
        for label in [titleLabel, overviewLabel, votingLabel, yearLabel] {
            label?.textColor = .white
        }
    }

    fileprivate func setupMovie() {

        guard let movie = movie else { return }
        title = movie.title
        titleLabel.text = movie.title
        overviewLabel.text = movie.overview
        votingLabel.text = movie.votesLabelText
        yearLabel.text = movie.releaseDate
    }

    // MARK: - Video player

    private func prepareVideoTrailer() {
        guard let movie = movie else { return }
        activityIndicator.startAnimating()

        TwoCodersAPIManager.shared.getVideoData(movieId: movie.id) { [weak self] videoList, error in
            guard let this = self, error == nil else {
                return
            }
            if let video = videoList.first {
                this.loadVideoTrailer(video: video)
            }
        }
    }

    private func loadVideoTrailer(video: VideoResult) {
        DispatchQueue.main.async {
            self.playerView.isHidden = false
            self.playerView.load(withVideoId: video.key)
            self.activityIndicator.stopAnimating()
        }
    }

    @IBAction func togglePlayVideo() {
        if !videoIsPlaying {
            DispatchQueue.main.async {
                self.playerView.playVideo()
                self.playButton.setTitle("Stop", for: .normal)
            }
        } else {
            DispatchQueue.main.async {
                self.playerView.stopVideo()
                self.playButton.setTitle("Start", for: .normal)
            }
        }
        videoIsPlaying.toggle()
    }
}

extension MovieDetailsViewController: YTPlayerViewDelegate {
    func playerView(_ playerView: YTPlayerView, didChangeTo state: YTPlayerState) {
        switch state {
        case .ended:
            DispatchQueue.main.async {
                self.playButton.setTitle("Start", for: .normal)
            }
        default:
            break
        }
    }

    func playerViewPreferredWebViewBackgroundColor(_ playerView: YTPlayerView) -> UIColor {
        return .black
    }
}
