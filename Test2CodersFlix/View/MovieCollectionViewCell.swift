//
//  MovieCollectionViewCell.swift
//  Test2C
//
//  Created by Dimitar Shopovski on 13.12.21.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {

    static let cellIdentifier = "MovieCollectionViewCellIdentifier"
    var fullHdLoaded = false
    weak var apiManager = TwoCodersAPIManager.shared
    var movie: Movie?

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var overlayView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        baseAppearance()
    }

    fileprivate func baseAppearance() {
        contentView.layer.borderWidth = 1.0
        contentView.layer.borderColor = UIColor.black.cgColor
        activityIndicator.startAnimating()

        titleLabel.layer.shadowColor = UIColor.black.cgColor
        titleLabel.layer.shadowOffset = CGSize(width: 1.0, height: 2.0)
        titleLabel.layer.shadowOpacity = 0.9
    }

    func configure(movie: Movie) {
        self.movie = movie
        titleLabel.text = movie.title
        descriptionLabel.text = movie.overview
        loadLowResolutionImage(path: movie.posterPath) { success in
            if success {
                self.loadHightResolutionImage(path: movie.posterPath)
            }
        }
    }
    
    override func prepareForReuse() {
        DispatchQueue.main.async {
            self.imageView.image = nil
        }
    }
}

extension MovieCollectionViewCell {
    private func loadLowResolutionImage(path: String, copmpletion: @escaping (Bool) -> Void) {
        // swiftlint:disable all line_length
        guard let baseUrl = apiManager?.imageConfig?.baseUrl, let sizes = apiManager?.imageConfig?.posterSizes, sizes.count > 0 else {
            copmpletion(false)
            return
        }
        let urlString = baseUrl + sizes[0] + path
        TwoCodersAPIManager.shared.fetchData(urlString: urlString) { (result: Result<UIImage?, Error>) in
            switch result {
            case .success(let image):
                copmpletion(true)
                DispatchQueue.main.async {
                    self.imageView.image = image
                    self.imageView.backgroundColor = .clear
                    self.imageView.contentMode = .scaleAspectFill
                }
            case .failure:
                copmpletion(false)
            }
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
            }
        }
    }

    private func loadHightResolutionImage(path: String) {
        guard let imageConfing = apiManager?.imageConfig else {
            return
        }
        let baseUrl = imageConfing.baseUrl
        let sizes = imageConfing.posterSizes
        guard sizes.count > 0, let last = sizes.last else {
            return
        }
        let urlString = baseUrl + last + path
        TwoCodersAPIManager.shared.fetchData(urlString: urlString) { (result: Result<UIImage?, Error>) in
            switch result {
            case .success(let image):
                DispatchQueue.main.async {
                    self.imageView.image = image
                    self.imageView.backgroundColor = .clear
                    self.imageView.contentMode = .scaleAspectFill
                }
            case .failure:
                break
            }
        }

    }
}
